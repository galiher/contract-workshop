package com.cloud.contracts.service;

import com.cloud.contracts.model.Team;
import com.cloud.contracts.repository.TeamsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TeamsService {

    private final TeamsRepository teamsRepository;

    public Team getSingleTeamById(String id) {
        return teamsRepository.getTeamById(id);
    }

    public void saveTeam(Team team) {
        teamsRepository.save(team);
    }
}
