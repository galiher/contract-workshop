package com.cloud.contracts.controller;

import com.cloud.contracts.model.Team;
import com.cloud.contracts.service.TeamsService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/teams")
public class TeamsController {

    private final TeamsService teamsService;

    public TeamsController(TeamsService teamsService) { this.teamsService = teamsService;}

    @GetMapping("/{id}")
    public Team getTeam(@PathVariable String id) {
        return teamsService.getSingleTeamById(id);
    }

    @PostMapping
    public void saveTeam(@RequestBody Team team) {
        teamsService.saveTeam(team);
    }
}
