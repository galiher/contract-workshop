package com.cloud.contracts.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.lang.NonNull;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Map;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonIgnoreProperties("hibernateLazyInitializer")
public class Plan {
    @Id
    private Integer id;

    @NonNull
    private String opponentId;

    @ElementCollection
    @Setter
    Map<String, String> players;

}
