package com.cloud.contracts.service;

import com.cloud.contracts.model.Opponent;
import com.cloud.contracts.model.Plan;
import com.cloud.contracts.repository.PlanRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PlanService {
    private final PlanRepository planRepository;
    private final TeamApi teamApi;

    public void savePlan(Plan plan) {
        Optional<Opponent> oppi = teamApi.getOpponent(plan.getOpponentId());

        Map<String, String> players = oppi.get().getPlayers();
        plan.setPlayers(players);

        planRepository.save(plan);


    }

    public Plan getPlanById(Integer id) {
        return planRepository.getOne(id);
    }
}
