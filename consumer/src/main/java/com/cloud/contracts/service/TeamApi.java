package com.cloud.contracts.service;

import com.cloud.contracts.model.Opponent;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "producer-feign-client", url = "${producer.url}")
public interface TeamApi {

    @GetMapping("/{id}")
    Optional<Opponent> getOpponent(@PathVariable("id") String id);
}
