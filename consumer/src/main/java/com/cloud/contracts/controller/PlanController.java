package com.cloud.contracts.controller;

import com.cloud.contracts.model.Plan;
import com.cloud.contracts.service.PlanService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/plans")
public class PlanController {
    private final PlanService planService;

    public PlanController(PlanService planService) {this.planService = planService;}

    @PostMapping
    public void createPlan(@RequestBody Plan plan) {

        planService.savePlan(plan);

    }

    @GetMapping("/{id}")
    public Plan getPlan(@PathVariable("id") Integer id) {
        return planService.getPlanById(id);
    }
}
